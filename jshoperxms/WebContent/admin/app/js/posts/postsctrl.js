define(['./module'],function(postsmodule){
	'use strict';

	/*=====End Of Page Element Directive=====*/
	
	/*=====Begin Of Save and Edit PostsT=====*/
	postsmodule.controller('posts',ServerSavePosts);
	function ServerSavePosts($scope,$http,$location){
		$scope.gtparams=[];
		$scope.index=1;
		//通过location中的operate参数区分操作行为
		var operate=$location.search().operate;
		if(operate!=undefined&&operate=="save"){
			//控制保存按钮显示
			$scope.savebtn={show:true};
			$scope.add={show:true};
			$scope.title='添加论坛数据&参数';
			$scope.status="1";
			//保存数据方法
			$scope.save=function(){
				if(validate()){
					var poststitle=$scope.poststitle;
					var content=$scope.content;
					var imageurl=$scope.imageurl;
					var status=$scope.status;
					$http({
						method:'POST',
						url:'../mall/feedback/posts/save.action',
						data:{
							'title':poststitle,
							'content':content,
							'imageurl':imageurl,
							'status':status
						}
					}).
					success(function(data,status,headers,config){
						if(data.sucflag){
							$scope.info={
									show:true,
									msg:'论坛信息添加成功'
							}
							$scope.errors={
									show:false
							}
						}
					}).
					error(function(data,status,headers,config){
						$scope.errors={
								show:true,
								msg:'系统异常'
						}
					});
				}
			}
		}
		//如果operate是edit则执行保存行为
		if(operate=='edit'){
			var id=$location.search().id;
			$scope.select={show:true};
			if(id!=undefined&&id!=""){
				$http({
					method:'POST',
					url:'../mall/feedback/posts/find.action',
					data:{
						'postsId':id
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.title='正在对'+data.bean.title+'进行编辑';
						$scope.headpath = data.bean.headpath;
						$scope.loginname = data.bean.loginname;
						$scope.createtime = data.bean.createtime;
						$scope.content=data.bean.content;
						$scope.poststitle=data.bean.title;
						$scope.bigimageurl = data.bean.bigimageurl;
						$scope.zannum = data.bean.zannum;
						$scope.replynum = data.bean.replynum;
						$scope.status=data.bean.status;
						$scope.updatebtn={show:true};
					}
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
				//更新数据方法
				$scope.update=function(){
					if(validate()){
						var status=$scope.status;
						$http({
							method:'POST',
							url:'../mall/feedback/posts/update.action',
							data:{
								'status':status,
								'postsId':id
							}
						}).
						success(function(data,status,headers,config){
							if(data.sucflag){
								$scope.info={
										show:true,
										msg:'论坛数据更新成功'
								}
							}
						}).
						error(function(data,status,headers,config){
							$scope.errors={
									show:true,
									msg:'系统异常'
							}
						});
					}
				}
			}
		}
		$scope.currentPage = 1;
		// 请求所有评论数据
		$scope.findAllPostsReply = function(){
			var id=$location.search().id;
			if(id==undefined){
				return false;
			}
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/findAllByPage.action',
				data:{
					'length':10,
					'postsId':id,
					'currentPage':1,
					'length':10
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.postsreplylist = data.beanlists;
					$scope.recordsTotal = data.recordsTotal;
					$scope.totalPage = data.totalPage;
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
		};
		// 首页
		$scope.load = function () {
			var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/findAllByPage.action',
				data:{
					'length':10,
					'postsId':id,
					'currentPage':1,
					'length':10
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.postsreplylist = data.beanlists;
					$scope.recordsTotal = data.recordsTotal;
					$scope.currentPage = currentPage;
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    // 下一页
		$scope.next = function () {
			if ($scope.currentPage < $scope.totalPage) {
	            var currentPage =  ++$scope.currentPage;
	        }
			var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/findAllByPage.action',
				data:{
					'length':10,
					'postsId':id,
					'currentPage':currentPage
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.postsreplylist = data.beanlists;
					$scope.currentPage = currentPage;
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    // 上一页
	    $scope.prev = function () {
	    	if ($scope.currentPage > 1) {
	            var currentPage = --$scope.currentPage;
	        }
	    	var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/findAllByPage.action',
				data:{
					'length':10,
					'postsId':id,
					'currentPage':currentPage
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.postsreplylist = data.beanlists;
					$scope.currentPage = currentPage;
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    // 最后一页
	    $scope.last = function () {
	    	if ($scope.currentPage > 1) {
	            var currentPage = $scope.totalPage;
	        }
	    	var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/findAllByPage.action',
				data:{
					'length':10,
					'postsId':id,
					'currentPage':currentPage
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.postsreplylist = data.beanlists;
					$scope.currentPage = currentPage;
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	 
	    // 显示回复输入框
	    $scope.comment = function(){
	    	$scope.savereply={show:true};
	    };
	    $scope.cancelcomment = function(){
	    	$scope.savereply={show:false};
	    };
	    // 显示回复他人输入框
	    $scope.replycomment = function(){
	    	$scope.savereplyother={show:true};
	    };
	    $scope.replycancelcomment = function(){
	    	$scope.savereplyother={show:false};
	    };
	    
	    // 回复帖子
	    $scope.addreply = function(){
			var replycontent=$scope.replycontent;
			if(replycontent==undefined||replycontent==""){
				$scope.errors={
						show:true,
						msg:"请输入回复内容"
				}
				return false;
			};
			var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postsreply/save.action',
				data:{
					'postsId':id,
					'replycontent':replycontent
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.info={
							show:true,
							msg:'回复信息添加成功'
					}
					window.location.reload() ;
					$scope.errors={
							show:false
					}
				}
			}).
			error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    // 判断当前人是否已点赞
	    $scope.findPostsZanRecord = function() {
	    	var id=$location.search().id;
	    	$http({
				method:'POST',
				url:'../mall/feedback/postszanrecord/find.action',
				data:{
					'postsId':id
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					if(data.like){
						$scope.donotlike = {show:true};
						$scope.postsZanRecordId = data.bean.id;
					} else {
						$scope.like = {show:true};
					}
				}
			}).error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    
	    $scope.addlike = function(){
	    	var id=$location.search().id;
			$http({
				method:'POST',
				url:'../mall/feedback/postszanrecord/save.action',
				data:{
					'postsId':id
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.info={
							show:true,
							msg:'点赞信息添加成功'
					}
					window.location.reload() ;
					$scope.errors={
							show:false
					}
				}
			}).
			error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    
	    $scope.donotlike = function(){
	    	var id=$scope.postsZanRecordId;
			$http({
				method:'POST',
				url:'../mall/feedback/postszanrecord/del.action',
				data:{
					'postsZanRecordId':id
				}
			}).
			success(function(data,status,headers,config){
				if(data.sucflag){
					$scope.info={
							show:true,
							msg:'点赞信息删除成功'
					}
					window.location.reload() ;
					$scope.errors={
							show:false
					}
				}
			}).
			error(function(data,status,headers,config){
				$scope.errors={
						show:true,
						msg:'系统异常'
				}
			});
	    };
	    
		//表单字段验证
		function validate(){
			var poststitle=$scope.poststitle;
			if(poststitle==undefined||poststitle==""){
				$scope.errors={
						show:true,
						msg:"请输入标题"
				}
				return false;
			};
			var content=$scope.content;
			if(content==undefined||content==""){
				$scope.errors={
						show:true,
						msg:"请输入内容"
				}
				return false;
			};
			return true;
		}
	}
	/*=====End Of Save and Edit PostsT=====*/
	
	/*=====Begin Of Find List PostsT=====*/
	postsmodule.controller('postslist',ServerPostsCtrl);
	//查询列表数据
	function ServerPostsCtrl($http,$location,$compile,$scope,$rootScope,$resource,DTOptionsBuilder,DTColumnBuilder,DTAjaxRenderer){
		var vm=this;
		vm.message='';
		vm.someClickHandler = someClickHandler;
		vm.selected={};
		vm.selectAll=false;
		vm.toggleAll=toggleAll;
		vm.toggleOne=toggleOne;
		vm.dtInstance={};
		var titleHtml='<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll,showCase.selected)">';
		vm.dtOptions=DTOptionsBuilder.newOptions().withOption('ajax',{
			type:'POST',
			url:'../mall/feedback/posts/findByPage.action',
			dataSrc:'data'
		})
		.withOption('processing',true)
		.withOption('paging',true)
		.withOption('serverSide',true)
		.withOption('createdRow',function(row,data,dataIndex){
			$compile(angular.element(row).contents())($scope);
		})
		.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('stateSave', true)
        .withOption('rowCallback',rowCallback)
		.withPaginationType('full_numbers')
		.withLanguageSource('./app/language/chinese.json')
		
		$scope.$on('handleRequest',function(){
			
		});
		$rootScope.getTableData=function serverData(){
			var req;
		}
		
		vm.dtColumns=[
		              DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable().renderWith(function(data,type,full,meta){
		            	  vm.selected[full.id]=false;
		            	  return '<input type="checkbox" ng-model="showCase.selected['+data.id+']" ng-click="showCase.toggleOne(showCase.selected)">';
		              }),
		              DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
		              DTColumnBuilder.newColumn('title').withTitle('标题').notSortable(),
			          DTColumnBuilder.newColumn('content').withTitle('内容').notSortable(),
			          DTColumnBuilder.newColumn('zannum').withTitle('点赞数').notSortable(),
			          DTColumnBuilder.newColumn('replynum').withTitle('回复数').notSortable(),
			          DTColumnBuilder.newColumn('nick').withTitle('发帖人昵称').notSortable(),
			          DTColumnBuilder.newColumn('loginname').withTitle('发帖人登录名').notSortable(),
			          DTColumnBuilder.newColumn('updatetime').withTitle('更新时间').notSortable(),
			          DTColumnBuilder.newColumn('activetime').withTitle('最后活跃时间').notSortable(),
			          DTColumnBuilder.newColumn('versiont').withTitle('版本号').notSortable(),
			          DTColumnBuilder.newColumn('status').withTitle('状态').notSortable(),
			          DTColumnBuilder.newColumn(null).withTitle('操作').notSortable().renderWith(actionHtml)];
		function actionHtml(data,type,full,meta){
			return '<button class="btn btn-warning" ng-click="edit('+data.id+')"><i class="fa fa-edit"></i></button>';
		}
		//表格中编辑按钮
		$scope.edit=function(id,name){
			$location.path('/posts').search({'operate':'edit','id':id});
		}
		
		/**
		 * 跳转到添加商品类型和参数页面
		 */
		$scope.save=function(){
			$location.path('/posts').search({'operate':'save'});
		}
		
		$scope.del=function(){
			var i=0;
			var ids=[];
			angular.forEach(vm.selected, function(data,index,array){
				if(data){
					i++;
					ids.push(index);
				}
			});
			if(i==0){
				$scope.errors={
						show:true,
						msg:'请选择一条记录'
				}
			}else{
				$scope.errors={
						show:false
				}
				//批量删除数据
				var idstrs=ids.join(",");
				$http({
					method:'POST',
					url:'../mall/feedback/posts/del.action',
					data:{
						'ids':idstrs
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.info={
								show:true,
								msg:'批量删除成功'
						}
						//window.location.reload();
					}
					
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
			}
		}

		/**
		 * 列表全选
		 */
		function toggleAll(selectAll,selectedItems){
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					selectedItems[id]=selectAll;
				}
			}
		}
		/**
		 * 列表单选
		 */
		function toggleOne(selectedItems){
			var me=this;
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					if(!selectedItems[id]){
						me.selectAll=false;
					}
				}
			}
			me.selectAll=true;
		}

		function someClickHandler(info) {
	        vm.message = info.id + ' - ' + info.name;
	    }
		/**
		 * 单击列表某行回调
		 */
		function rowCallback(nRow,aData,iDisplayIndex,iDisplayIndexFull){
			$('td', nRow).unbind('click');
	        $('td', nRow).bind('click', function() {
	            $scope.$apply(function() {
	                vm.someClickHandler(aData);
	            });
	        });
	        return nRow;
		}
		
	}
	/*=====End Of Save and List PostsT=====*/
});
