define([ 'angular' ], function(angular) {
	'use strict';

	var myinterceptors = angular.module('myinterceptors', []);
	
	myinterceptors.factory('sessionInterceptor',['$window','$location','sessionService',function($window,$location,sessionService){
		var sessionInjector={
				request:function(config){
					console.log(config.url);
					if(!sessionService.gologin){
						config.headers['x-session-token'] = sessionService.xsessiontoken;
					}
					return config;
				},
				response:function(response){
					//判定服务器session失效后进行页面登陆跳转
					if(response.data.sessionTimeOut!=undefined&&response.data.sessionTimeOut!=null&&response.data.sessionTimeOut){
						window.location.href='login.html';
					}
					return response;
				},
				responseError:function(response){
					window.location.href='login.html';
					return response;
				},
				requestError:function(request){
					window.location.href='login.html';
					return request;
				}

		};
		return sessionInjector;
	}]);
	myinterceptors.config(['$httpProvider',function($httpProvider){
		$httpProvider.interceptors.push('sessionInterceptor');
	}]);
	
	
});
