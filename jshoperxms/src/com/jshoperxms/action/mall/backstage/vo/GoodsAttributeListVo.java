package com.jshoperxms.action.mall.backstage.vo;

import java.io.Serializable;

/**
 * 商品属性列表
* @ClassName: GoodsAttributeListVo 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月9日 下午4:12:50 
*
 */
public class GoodsAttributeListVo implements Serializable{

	private String id;
	private String attrname;
	private int sort;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	

	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getAttrname() {
		return attrname;
	}
	public void setAttrname(String attrname) {
		this.attrname = attrname;
	}

	
}
