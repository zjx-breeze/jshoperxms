package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.TemplateT;
import com.jshoperxms.service.TemplateTService;
@Service("templateTService")
@Scope("prototype")
public class TemplateTServiceImpl extends BaseTServiceImpl<TemplateT> implements TemplateTService {

}
