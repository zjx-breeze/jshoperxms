package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.MemberMgRpTDao;
import com.jshoperxms.entity.MemberMgRpT;

@Repository("memberMgRpTDao")
public class MemberMgRpTDaoImpl extends BaseTDaoImpl<MemberMgRpT> implements MemberMgRpTDao {

	private static final Logger log = LoggerFactory.getLogger(GlobalParamMDaoImpl.class);

}
