package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.PostsReplyTDao;
import com.jshoperxms.entity.PostsReplyT;

@Repository("postsReplyTDao")
public class PostsReplyTDaoImpl extends BaseTDaoImpl<PostsReplyT>  implements PostsReplyTDao {

	private static final Logger log = LoggerFactory.getLogger(PostsReplyTDaoImpl.class);

}
